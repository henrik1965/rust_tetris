#![allow(unused_must_use)]
#![allow(unused_imports)]
#![allow(dead_code)]
#![allow(unused_mut)]
use self::{pieces::Place, Direction::*};
use crossterm::{style::Color, terminal::disable_raw_mode};
use std::io::Read;
use std::slice::Iter;
use std::thread::sleep;
use std::time;
mod drawing;
mod pieces;
mod primitivs;

//
// https://www.youtube.com/watch?v=O0gAgQQHFcQ
// ▉▉
// ТЕТРИС
//     ▉▉
/*

     <! . . . . . . . . . .!>
     <! . . . . . . . . . .!>
     <! . . .▉▉▉▉▉▉ . . . .!>
     <! . . .▉▉ . . . . . .!>
     <! . . . . . . . . . .!>
     <! . . . . . . . . . .!>
     <!====================!>
       \/\/\/\/\/\/\/\/\/\/

*/

pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub fn iterator() -> Iter<'static, Direction> {
        static DIRECTIONS: [Direction; 4] = [Right, Up, Left, Down];
        DIRECTIONS.iter()
    }
}
pub struct Game {
    board: Vec<Color>,
    width: u16,
    height: u16,
    wall_color: Color,
    mortar_color: Color,
    play_width: u16,
    play_height: u16,
    play_x: u16,
    play_y: u16,
    block_width: u16,
    block_height: u16,
}

impl Game {
    pub fn new() -> Game {
        let (cols, rows) = primitivs::getsize();
        let board = Vec::new();
        let mut g = Game {
            board,
            height: rows,
            width: cols,
            play_width: 10,
            play_height: 20,
            wall_color: Color::Grey,
            mortar_color: Color::White,
            play_x: 0,
            play_y: 0,
            block_width: 4,
            block_height: 2,
        };
        if (g.height < g.play_height * g.block_height) || (g.width < g.play_width * g.block_width) {
            g.block_height = 1;
            g.block_width = 2;
        }
        let tmp: i16 = (g.width as i16 - (g.play_width as i16 * g.block_width as i16)) / 2;
        g.play_x = if tmp > 0 { tmp as u16 } else { 1 };
        let tmp: i16 = (g.height as i16 - g.play_height as i16 * g.block_height as i16) / 2;
        g.play_y = if tmp > 0 { tmp as u16 } else { 1 };
        if g.play_height * g.block_height > g.height {
            g.play_height = g.height / g.block_height - 1;
        }
        g.clear();
        g
    }

    pub fn clear(&mut self) {
        self.board.clear();
        for _ in 0..(self.play_height * self.play_width) {
            self.board.push(self.wall_color);
        }
    }

    pub fn run(&mut self) {
        primitivs::cursor(false);
        //enable_raw_mode().unwind();
        self.background();

        let mut game_over = false;
        let mut piece = 1;
        let mut rot = Right;
        let mut col = self.play_width / 2 - 2;
        let mut col_dir: i16 = 1;
        let mut row = 0;
        let mut old_row = row;
        let mut old_col = col;

        while !game_over {
            self.erase_piece(piece, &rot, old_col, old_row);
            row += 1;
            let rc = self.test_piece(piece, &rot, col, row);

            match rc {
                Place::Ok => {
                    old_row = row;
                    old_col = col;
                    self.draw_piece(piece, &rot, col, row);
                }
                Place::Other => {
                    self.draw_piece(piece, &rot, old_col, old_row);
                    if row <= 1 {
                        game_over = true;
                    }
                    row = 0;
                    old_row = 999;
                    piece += 1;
                    if piece >= self.max_piece() {
                        piece = 0;
                    }
                }
                Place::Right => {
                    self.draw_piece(piece, &rot, old_col, old_row);
                }
                Place::Bottom => {
                    self.draw_piece(piece, &rot, old_col, old_row);
                    row = 0;
                    old_row = 999;
                    piece += 1;
                    if piece >= self.max_piece() {
                        piece = 0;
                    }
                }
                Place::Top => {}
                Place::Left => {}
            }
            self.board();
            self.home();

            //println!("{:?}", print_events());
            let new_col = (col as i16 + col_dir) as u16;
            let rc = self.test_piece(piece, &rot, new_col, row);
            if rc == Place::Ok {
                col = new_col;
                if new_col == 0 || new_col == self.play_width {
                    col_dir *= -1;
                }
            } else if rc == Place::Right {
                col_dir *= -1;
            } else if rc == Place::Other {
                col_dir *= -1;
            }
            sleep(time::Duration::from_millis(100));
        }

        // let mut row = 0;
        // for i in Direction::iterator() {
        //     for j in 0..7 {
        //         self.piece(j, i, (j % 2) * 5, row + j / 2 * 5, true);
        //     }
        //     self.board();
        //     self.home();
        //     for j in 0..7 {
        //         self.piece(j, i, (j % 2) * 5, row + j / 2 * 5, false);
        //     }
        //     sleep(time::Duration::from_millis(1000));
        //     row += 1;
        // }
        self.home();
        disable_raw_mode();
        primitivs::cursor(true);
    }
}

use std::time::Duration;

use crossterm::event::{poll, read, Event};

fn print_events() -> crossterm::Result<()> {
    // `poll()` waits for an `Event` for a given time period
    if poll(Duration::from_millis(10))? {
        // It's guaranteed that the `read()` won't block when the `poll()`
        // function returns `true`
        match read()? {
            Event::Key(event) => println!("{:?}", event),
            Event::Mouse(event) => println!("{:?}", event),
            Event::Resize(width, height) => println!("New size {}x{}", width, height),
        }
    } else {
        // Timeout expired and no `Event` is available
    }
    Ok(())
}

// End of file
