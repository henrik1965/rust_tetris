use super::{primitivs, Game};

// ╋┻┳┃┣┫━┏┓┗┛█▓
// ┳━━━━┻━━━━┳━━━━┻
// ┃         ┃
// ┻━━━━┳━━━━┻━━━━┳
//      ┃         ┃
// ┳━━━━┻━━━━┳━━━━┻
// ┃         ┃
//
// ┳━━┻━━┳━━┻
// ┻━━┳━━┻━━┳
// ┳━━┻━━┳━━┻

impl Game {
    pub fn board(&self) {
        let h = self.play_height * self.block_height;
        let w = self.play_width * self.block_width;
        for y in (0..h).step_by(1) {
            for x in (0..w).step_by(1) {
                let _ = primitivs::put(
                    " ",
                    self.play_x + x,
                    self.play_y + y,
                    self.mortar_color,
                    self.board[((y / self.block_height) * self.play_width + x / self.block_width)
                        as usize],
                );
            }
        }
        let _ = primitivs::flush();
    }

    pub fn background(&self) {
        let _ = primitivs::clear();
        const WALL: &'static [&'static str] = &[
            "┻━━┳━━",
            "┳━━┻━━",
            "┻━━━━┳━━━━",
            "     ┃    ",
            "┳━━━━┻━━━━",
            "┃         ",
        ];
        let wb = self.block_width * 2 + 2;
        let o = if self.block_height == 1 { 0 } else { 2 };

        for y in (0..self.height).step_by(self.block_height as usize * 2) {
            for x in (0..(self.width / wb) * wb).step_by(wb as usize) {
                for i in 0..self.block_height * 2 {
                    if y + i < self.height {
                        let _ = primitivs::put(
                            WALL[(i + o) as usize],
                            x,
                            y + i,
                            self.mortar_color,
                            self.wall_color,
                        );
                    }
                }
            }
        }

        let h = self.play_height * self.block_height + 2;
        let w = self.play_width * self.block_width + 2;
        for y in (0..h).step_by(1) {
            for x in (0..w).step_by(1) {
                let mut block = " ";
                if (x == 0) && (y == 0) {
                    block = "┏";
                } else if (x == w - 1) && (y == 0) {
                    block = "┓";
                } else if (x == 0) && (y == h - 1) {
                    block = "┻"; //"┗";
                } else if (x == w - 1) && (y == h - 1) {
                    block = "┻"; //"┛";
                } else if (x == 0) || (x == w - 1) {
                    if y % self.block_height != ((self.height + 1) % self.block_height)
                        && self.block_height != 1
                    {
                        block = "┃";
                    } else {
                        if x == 0 {
                            block = "┫";
                        } else {
                            block = "┣";
                        }
                    }
                } else if (y == 0) || (y == h - 1) {
                    block = "━";
                    if y == 0 {
                        if (x + self.play_x) % wb == self.play_x % wb {
                            block = "┻";
                        }
                    } else {
                        if x % wb == ((self.play_x + 2 + (self.height % 2) * 5) % 10) {
                            block = "┳";
                        }
                    }
                }
                let _ = primitivs::put(
                    block,
                    self.play_x - 1 + x,
                    self.play_y - 1 + y,
                    self.mortar_color,
                    self.wall_color,
                );
            }
        }
        let _ = primitivs::flush();
    }

    pub fn home(&self) {
        let _ = primitivs::goto(
            0,
            (self.height + self.play_height * self.block_height) / 2 + 1,
        );
    }
}

// End of file
