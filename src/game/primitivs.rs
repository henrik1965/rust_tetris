use crossterm::{
    cursor,
    style::{self, style, Color},
    terminal, ExecutableCommand, QueueableCommand, Result,
};
use cursor::{Hide, Show};
use std::io;
use std::io::Write;
use terminal::size;

pub fn put(s: &str, x: u16, y: u16, f: Color, b: Color) -> Result<()> {
    let mut stdout = io::stdout();
    stdout
        .queue(cursor::MoveTo(x, y))?
        .queue(style::PrintStyledContent(style(s).with(f).on(b)))?;
    Ok(())
}

pub fn goto(x: u16, y: u16) -> Result<()> {
    let mut stdout = io::stdout();
    stdout.queue(cursor::MoveTo(x, y))?;
    Ok(())
}

pub fn flush() -> Result<()> {
    let mut stdout = std::io::stdout();
    stdout.flush()?;
    Ok(())
}

pub fn clear() -> Result<()> {
    let mut stdout = &io::stdout();
    stdout.execute(terminal::Clear(terminal::ClearType::All))?;
    Ok(())
}

pub fn getsize() -> (u16, u16) {
    let (cols, rows) = size().unwrap();
    (cols, rows)
}

pub fn cursor(show: bool) {
    let mut stdout = &io::stdout();
    if show {
        let _ = stdout.execute(Show);
    } else {
        let _ = stdout.execute(Hide);
    }
}

// End of file
