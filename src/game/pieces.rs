use super::Direction::*;
use super::Game;
use crossterm::style::Color;

// ╋┻┳┃┣┫━┏┓┗┛█▓
// ┳━━━━┻━━━━┳━━━━┻
// ┃         ┃
// ┻━━━━┳━━━━┻━━━━┳
//      ┃         ┃
// ┳━━━━┻━━━━┳━━━━┻
// ┃         ┃
//
// https://www.youtube.com/watch?v=O0gAgQQHFcQ
// ▉▉
// ТЕТРИС
//     ▉▉
/*

     <! . . . . . . . . . .!>
     <! . . . . . . . . . .!>
     <! . . .▉▉▉▉▉▉ . . . .!>
     <! . . .▉▉ . . . . . .!>
     <! . . . . . . . . . .!>
     <! . . . . . . . . . .!>
     <!====================!>
       \/\/\/\/\/\/\/\/\/\/

*/

#[derive(PartialEq)]
pub enum Place {
    Ok,
    Left,
    Right,
    Top,
    Bottom,
    Other,
}

impl Game {
    fn block(&mut self, at_x: u16, at_y: u16, c: Color) -> Place {
        if at_x >= self.play_width {
            return Place::Right;
        }
        let index = (at_y * self.play_width + at_x) as usize;
        if index >= self.board.len() {
            return Place::Bottom;
        }
        if c != self.wall_color && self.board[index] != self.wall_color && self.board[index] != c {
            return Place::Other;
        }
        if c != Color::Reset {
            self.board[index] = c;
        }
        Place::Ok
    }

    pub fn max_piece(&mut self) -> u16 {
        7
    }

    pub fn create_piece(&mut self, ty: u16, rot: &super::Direction) -> (Color, [u16; 4], [u16; 4]) {
        let mut color;
        let coords_x: [u16; 4];
        let coords_y: [u16; 4];
        match ty {
            0 => {
                // * *
                // * *
                color = Color::Yellow;
                coords_x = [1, 1, 2, 2];
                coords_y = [1, 2, 1, 2];
            }
            1 => {
                color = Color::Cyan;
                match rot {
                    // * * * *
                    //
                    Right => {
                        coords_x = [0, 1, 2, 3];
                        coords_y = [1, 1, 1, 1];
                    }
                    //     *
                    //     *
                    //     *
                    //     *
                    Down => {
                        coords_x = [2, 2, 2, 2];
                        coords_y = [0, 1, 2, 3];
                    }
                    //
                    // * * * *
                    Left => {
                        coords_x = [0, 1, 2, 3];
                        coords_y = [2, 2, 2, 2];
                    }
                    //   *
                    //   *
                    //   *
                    //   *
                    Up => {
                        coords_x = [1, 1, 1, 1];
                        coords_y = [0, 1, 2, 3];
                    }
                }
            }
            2 => {
                color = Color::Blue;
                match rot {
                    //
                    // * * *
                    //     *
                    Right => {
                        coords_x = [0, 1, 2, 2];
                        coords_y = [1, 1, 1, 2];
                    }
                    //   * *
                    //   *
                    //   *
                    Up => {
                        coords_x = [1, 2, 1, 1];
                        coords_y = [0, 0, 1, 2];
                    }
                    // *
                    // * * *
                    //
                    Left => {
                        coords_x = [0, 0, 1, 2];
                        coords_y = [0, 1, 1, 1];
                    }
                    //   *
                    //   *
                    // * *
                    Down => {
                        coords_x = [0, 1, 1, 1];
                        coords_y = [2, 0, 1, 2];
                    }
                }
            }
            3 => {
                color = Color::AnsiValue(214);
                match rot {
                    //
                    // * * *
                    // *
                    super::Direction::Right => {
                        coords_x = [0, 1, 2, 0];
                        coords_y = [1, 1, 1, 2];
                    }
                    //   *
                    //   *
                    //   * *
                    super::Direction::Up => {
                        coords_x = [1, 1, 1, 2];
                        coords_y = [0, 1, 2, 2];
                    }
                    //     *
                    // * * *
                    super::Direction::Left => {
                        coords_x = [2, 0, 1, 2];
                        coords_y = [0, 1, 1, 1];
                    }
                    // * *
                    //   *
                    //   *
                    super::Direction::Down => {
                        coords_x = [0, 1, 1, 1];
                        coords_y = [0, 0, 1, 2];
                    }
                }
            }
            4 => {
                color = Color::Red;
                match rot {
                    // * *
                    //   * *
                    Right => {
                        coords_x = [0, 1, 1, 2];
                        coords_y = [0, 0, 1, 1];
                    }
                    //   *
                    // * *
                    // *
                    Up => {
                        coords_x = [1, 0, 1, 0];
                        coords_y = [0, 1, 1, 2];
                    }
                    //
                    // * *
                    //   * *
                    Left => {
                        coords_x = [0, 1, 1, 2];
                        coords_y = [1, 1, 2, 2];
                    }
                    //     *
                    //   * *
                    //   *
                    Down => {
                        coords_x = [2, 1, 2, 1];
                        coords_y = [0, 1, 1, 2];
                    }
                }
            }
            5 => {
                color = Color::Green;
                match rot {
                    //   * *
                    // * *
                    Right => {
                        coords_x = [1, 2, 0, 1];
                        coords_y = [0, 0, 1, 1];
                    }
                    // *
                    // * *
                    //   *
                    Up => {
                        coords_x = [0, 0, 1, 1];
                        coords_y = [0, 1, 1, 2];
                    }
                    //
                    //   * *
                    // * *
                    Left => {
                        coords_x = [1, 2, 0, 1];
                        coords_y = [1, 1, 2, 2];
                    }
                    //   *
                    //   * *
                    //     *
                    Down => {
                        coords_x = [1, 1, 2, 2];
                        coords_y = [0, 1, 1, 2];
                    }
                }
            }
            6 => {
                color = Color::Magenta;
                match rot {
                    // * * *
                    //   *
                    Right => {
                        coords_x = [0, 1, 2, 1];
                        coords_y = [1, 1, 1, 2];
                    }
                    //   *
                    //   * *
                    //   *
                    Up => {
                        coords_x = [1, 1, 2, 1];
                        coords_y = [0, 1, 1, 2];
                    }
                    //   *
                    // * * *
                    Left => {
                        coords_x = [1, 0, 1, 2];
                        coords_y = [0, 1, 1, 1];
                    }
                    //   *
                    // * *
                    //   *
                    Down => {
                        coords_x = [1, 0, 1, 1];
                        coords_y = [0, 1, 1, 2];
                    }
                }
            }
            _ => {
                color = Color::Black;
                coords_x = [0, 0, 0, 0];
                coords_y = [0, 0, 0, 0];
            }
        }
        (color, coords_x, coords_y)
    }

    pub fn test_piece(&mut self, ty: u16, rot: &super::Direction, at_x: u16, at_y: u16) -> Place {
        let (_, coords_x, coords_y) = self.create_piece(ty, rot);

        for i in 0..4 {
            let rc = self.block(at_x + coords_x[i], at_y + coords_y[i], Color::Reset);
            if rc != Place::Ok {
                return rc;
            }
        }
        Place::Ok
    }

    pub fn draw_piece(&mut self, ty: u16, rot: &super::Direction, at_x: u16, at_y: u16) {
        let (color, coords_x, coords_y) = self.create_piece(ty, rot);
        for i in 0..4 {
            self.block(at_x + coords_x[i], at_y + coords_y[i], color);
        }
    }

    pub fn erase_piece(&mut self, ty: u16, rot: &super::Direction, at_x: u16, at_y: u16) {
        let (_, coords_x, coords_y) = self.create_piece(ty, rot);
        for i in 0..4 {
            self.block(at_x + coords_x[i], at_y + coords_y[i], self.wall_color);
        }
    }
}

// End of file
